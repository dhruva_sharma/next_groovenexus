import matter from 'gray-matter'
import Link from 'next/link';
import {useState} from 'react';
import Image from 'next/image'; 
import banner from '../public/images/banner.png'
import brochure from '../public/images/brochure.png'; 
import BlogListcard from '../components/BlogListCard';
import Navbar from './post/Navbar'
import Footer from './post/Footer';
import { BiAlarm, BiHeadphone, BiSearchAlt2 } from "react-icons/bi";

function Posts(props){


    console.debug("Blog page:",props)
    const [blogList,setBlogList] =useState(props.allBlogs);
    return (
            <div>
                <Navbar/>
                <div className="img">
                <Image src={banner} alt="banner" layout="responsive" />
                </div>
                <div className="container">
                    <div className="row ">
                        <div className="col-md-12">
                            <div className="p-2">
                            <form className="d-xl-none">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search..."/>
                                    <button className="input-group-text btn-danger"><BiSearchAlt2/></button>
                                </div>
                            </form>
                            </div>
                            <h2>Top Artist</h2>
                            <p>Get to know about the best clubs, worldwide, offering unique, mindblowing experiences across music and fine-dining.</p>
                        </div>                    
                    </div>
                </div>
                <div className="container-lg container-xl">
                    <div className="row">
                        <div className="col-md-8">
                            <div className="row">
                            {blogList.map(d=>{
                                return (
                                    <Link key={d.slug} href={{ pathname: `/posts/${d.slug}` }}>
                                    {d? <BlogListcard data={d}/>:""}
                                    </Link>)
                                })}
                            
                            </div>
                        </div>
                        <div className="col-md-4 p-2">
                            <div className="card shadow">
                                <div className="card-body">
                                    <h4 className=" border-left">WHAT'S GROOVIN'?</h4>
                                    <p className="card-text">What are Scales in Music: A Beginner's 
                                        Guide & Types</p>
                                        <span className="d-flex text-mute border-bottom"><p className="me-4"><BiAlarm className="me-2"/>10 June 2021</p><p><BiHeadphone className="me-2"/>GrooveClasses</p></span>
                                        <p className="card-text">What are Scales in Music: A Beginner's 
                                            Guide & Types</p>
                                            <span className="d-flex text-mute border-bottom"><p className="me-4"><BiAlarm className="me-2"/>10 June 2021</p><p><BiHeadphone className="me-2"/>GrooveClasses</p></span>
                                            <p className="card-text">What are Scales in Music: A Beginner's 
                                                Guide & Types</p>
                                                <span className="d-flex text-mute border-bottom"><p className="me-4"><BiAlarm className="me-2"/>10 June 2021</p><p><BiHeadphone className="me-2"/>GrooveClasses</p></span>
                                                <p className="card-text">What are Scales in Music: A Beginner's 
                                                    Guide & Types</p>
                                                    <span className="d-flex text-mute border-bottom"><p className="me-4"><BiAlarm className="me-2"/>10 June 2021</p><p><BiHeadphone className="me-2"/>GrooveClasses</p></span>
                                                
                                </div>
                            </div>
                            <div className="col-md-12 d-md-block d-none d-sm-block d-sm-none mt-3 ">
                                        <Image src={brochure} alt="brochure" className="w-100"/>

                            </div>
                        </div>
                    </div>
                </div>
                <Footer />
            </div>

    )
}

export default Posts;

export async function getStaticProps() {
    const siteConfig = await import(`../data/config.json`)
    //get posts & context from folder
    const posts = (context => {
      const keys = context.keys()
      const values = keys.map(context)
  
      const data = keys.map((key, index) => {
        // Create slug from filename
        const slug = key
          .replace(/^.*[\\\/]/, '')
          .split('.')
          .slice(0, -1)
          .join('.')
        const value = values[index]
        // Parse yaml metadata & markdownbody in document
        const document = matter(value.default)
        return {
          frontmatter: document.data,
          markdownBody: document.content,
          slug,
        }
      })
      return data
    })(require.context('../posts', true, /\.md$/))
  
    return {
      props: {
        allBlogs: posts,
        title: siteConfig.default.title,
        description: siteConfig.default.description,
      },
    }
  }
  