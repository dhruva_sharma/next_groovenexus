
import Image from 'next/image';
import logo from '../../public/images/groovenexus-logo.webp'; 
import { BiSearchAlt2 } from "react-icons/bi";

function Navbar() {
  return (
            <div>
                <header className="bg-white shadow border-bottom pt-2 fixed-top">
                    <div className="container-fluid">
                    <nav className="container-lg justify-content-lg-around navbar-expand-xl navbar-light">
                        <div className="row justify-content-center">
                            <div className="col-md-12">
                                {/* <nav className="container-lg container-xl navbar navbar-expand-xl navbar-light"> */}
                                    <div className="row">
                                        <div className="col-md-4 p-0">
                                            <a href="https://nextgroovenexus.tinggit.com/">
                                                <Image src={logo} alt="groovenexus-logo" className="w-100"/>
                                            </a>  
                                            <button  className="border-0 d-md-none d-sm-block float-end navbar-toggler  mt-3" data-bs-toggle="collapse" data-bs-target="#navBarTarget">
                                            <span className="navbar-toggler-icon">
                                            </span>
                                        </button>  
                                        </div>
                                        {/* <button  className="navbar-toggler d-inline-block d-sm-none d-none  border-0" data-bs-toggle="collapse" data-bs-target="#navBarTarget">
                                            <span className="navbar-toggler-icon">
                                            </span>
                                        </button>   */}
                                        <div className="col-md-8 text-end">
                                        <button  className="border-0 collapsed d-lg-block d-md-block d-none mt-3 d-sm-none d-xl-block d-xl-none ms-auto navbar-toggler" data-bs-toggle="collapse" data-bs-target="#navBarTarget">
                                            <span className="navbar-toggler-icon">
                                            </span>
                                        </button>  
                                            <div className="row">
                                                <div className="col-md-12">
                                                <div className="d-lg-none d-md-none d-none d-sm-none d-xl-flex justify-content-end row">
                                                <div className="col-md-5">
                                                    <form>
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" placeholder="Search..."/>
                                                            <button className="input-group-text btn-danger"><BiSearchAlt2/></button>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div className="col-md-2 p-0 text-start">
                                                <button className="btn btn-danger nav-button">Write for us</button>
                                                </div>
                                                </div>
                                                </div>
                                                <div className="col-md-12">
                                                <div className="collapse navbar-collapse justify-content-end " id="navBarTarget">
                                                    <ul className=" pt-2 navbar-nav ">
                                                        <li className="nav-item">
                                                            <a className="nav-link text-dark typo" href="">HOME</a>
                                                        </li>
                                                        <li className="nav-item">
                                                            <a className="nav-link text-dark typo " href="">ARTIST SPOTLIGHT</a>
                                                        </li>
                                                        <li className="nav-item">
                                                            <a className="nav-link text-dark typo" href="">ARTIST STORIES</a>
                                                        </li>
                                                        <li className="nav-item">
                                                            <a className="nav-link text-dark typo" href="">LISTINGS</a>
                                                        </li>
                                                        <li className="nav-item">
                                                            <a className="nav-link text-dark typo" href="">NEWS</a>
                                                        </li>
                                                        <li className="nav-item">
                                                            <a className="nav-link text-dark typo" href="">REVIEW</a>
                                                        </li>
                                                        <li className="nav-item">
                                                            <a className="nav-link text-dark typo" href="">GROOVECLASSES</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                {/* </nav> */}
                            </div>
                        </div>
                        </nav>
                    </div>
                </header>
            </div>
  )
}


export default Navbar