import matter from 'gray-matter'
import ReactMarkdown from 'react-markdown'
import Image from 'next/image';
import logo from '../../public/images/groovenexus-logo.webp'; 
import footerImg from '../../public/images/footerImg.png'; 
import banner from '../../public/images/banner.png'; 
import brochure from '../../public/images/brochure.png'; 
import ministryOfSound from '../../public/images/ministryOfSound.png'; 

const glob = require('glob')


function PostDetail(props) {
  console.debug("Blog Detail page:",props)


  return (

    <div>
                <header className="bg-white fixed-top">
                <div className="container-fluid">
                    <div className="row justify-content-center">
                        <div className="col-md-12">
                            <nav className="container-lg container-xl navbar navbar-expand-xl navbar-light">
                                <a href="#">
                                    <Image src={logo} alt="groovenexus-logo" className="w-100"/>

                                </a>
                                    <button  className="navbar-toggler  border-0" data-toggle="collapse" data-target="#nav">
                            
                                        <span className="navbar-toggler-icon"></span>
                                        
                                    </button>
                                    <div className="collapse navbar-collapse justify-content-end " id="nav">
                                        <ul className="max-auto navbar-nav ">
                                            <li className="nav-item">
                                                <a className="nav-link text-dark" href="">HOME</a>
                                            </li>
                                            <li className="nav-item">
                                                <a className="nav-link text-dark" href="">ARTIST SPOTLIGHT</a>
                                            </li>
                                            <li className="nav-item">
                                                <a className="nav-link text-dark" href="">ARTIST STORIES</a>
                                            </li>
                                            <li className="nav-item">
                                                <a className="nav-link text-dark" href="">LISTINGS</a>
                                            </li>
                                            <li className="nav-item">
                                                <a className="nav-link text-dark" href="">NEWS</a>
                                            </li>
                                            <li className="nav-item">
                                                <a className="nav-link text-dark" href="">REVIEW</a>
                                            </li>
                                            <li className="nav-item">
                                                <a className="nav-link text-dark" href="">GROOVECLASSES</a>
                                            </li>
                                        </ul>
                                    </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </header>
        <section>
        <Image src={banner} alt="banner" className="w-100 img pb-1"/>
        </section>
  
        <section>
            <div className="container pt-5 mt-5">
                <div className="row pt-5">
                    <div className="col-md-12 pt-5 mt-5">
                        <h2>Top Artist</h2>
                        <p>Get to know about the best clubs, worldwide, offering unique, mindblowing experiences across music and fine-dining.</p>
                    </div>                    
                </div>
            </div>
            <div className="container-lg container-xl">
                <div className="row">
                    <div className="col-md-8">
                        <div className="row">
                            <div className="col-md-6 p-2">
                                <div className="card">
                                    <Image src={ministryOfSound} alt="ministryOfSound" className="card-img-top"/>

                                    <div className="card-body">
                                        <span className="d-flex"><p><i className="far fa-clock"></i>10 June 2021</p><p><i className="fas fa-headphones-alt"></i>Top Club</p></span>
                                      <h6 className="card-title">Enjoy Amazing Music & Great Ambience At
                                        Ministry Of Sound Noida, Gardens Galleria</h6>
                                      <p className="card-text">Ministry of Sound began as the idea of Justin Berkmann. It was inspired by New York’s Paradise Garage- which he described as “an amazing club”. It had lights, darkness, mus...</p>
                                      <span className="d-flex"><p className="col-md-6 p-0"><i className="fas fa-star text-warning"></i><i className="fas fa-star text-warning"></i><i className="fas fa-star text-warning"></i><i className="fas fa-star text-warning"></i><i className="fas fa-star text-warning"></i></p><p className="col-md-6 pl-lg-5">3 min read</p></span>
                                      <a href="#" >Read more</a>
                                    </div>
                                  </div>
                            </div>
                            <div className="col-md-6 p-2">
                                <div className="card shadow">
                                    <Image src={ministryOfSound} alt="ministryOfSound" className="card-img-top"/>

                                    <div className="card-body">
                                        <span className="d-flex"><p><i className="far fa-clock"></i>10 June 2021</p><p><i className="fas fa-headphones-alt"></i>Top Club</p></span>
                                      <h6 className="card-title">Enjoy Amazing Music & Great Ambience At
                                        Ministry Of Sound Noida, Gardens Galleria</h6>
                                      <p className="card-text">Ministry of Sound began as the idea of Justin Berkmann. It was inspired by New York’s Paradise Garage- which he described as “an amazing club”. It had lights, darkness, mus...</p>
                                      <span className="d-flex"><p className="col-md-6 p-0"><i className="fas fa-star text-warning"></i><i className="fas fa-star text-warning"></i><i className="fas fa-star text-warning"></i><i className="fas fa-star text-warning"></i><i className="fas fa-star text-warning"></i></p><p className="col-md-6 pl-lg-5">3 min read</p></span>
                                      <a href="#" >Read more</a>
                                    </div>
                                  </div>
                            </div>
                            <div className="col-md-6 p-2">
                                <div className="card shadow">
                                    <Image src={ministryOfSound} alt="ministryOfSound" className="card-img-top"/>

                                    <div className="card-body">
                                        <span className="d-flex"><p><i className="far fa-clock"></i>10 June 2021</p><p><i className="fas fa-headphones-alt"></i>Top Club</p></span>
                                      <h6 className="card-title">Enjoy Amazing Music & Great Ambience At
                                        Ministry Of Sound Noida, Gardens Galleria</h6>
                                      <p className="card-text">Ministry of Sound began as the idea of Justin Berkmann. It was inspired by New York’s Paradise Garage- which he described as “an amazing club”. It had lights, darkness, mus...</p>
                                      <span className="d-flex"><p className="col-md-6 p-0"><i className="fas fa-star text-warning"></i><i className="fas fa-star text-warning"></i><i className="fas fa-star text-warning"></i><i className="fas fa-star text-warning"></i><i className="fas fa-star text-warning"></i></p><p className="col-md-6 pl-lg-5">3 min read</p></span>
                                      <a href="#" >Read more</a>
                                    </div>
                                  </div>
                            </div>
                            <div className="col-md-6 p-2">
                                <div className="card shadow">
                                    <Image src={ministryOfSound} alt="ministryOfSound" className="card-img-top"/>

                                    <div className="card-body">
                                        <span className="d-flex"><p><i className="far fa-clock"></i>10 June 2021</p><p><i className="fas fa-headphones-alt"></i>Top Club</p></span>
                                      <h6 className="card-title">Enjoy Amazing Music & Great Ambience At
                                        Ministry Of Sound Noida, Gardens Galleria</h6>
                                      <p className="card-text">Ministry of Sound began as the idea of Justin Berkmann. It was inspired by New York’s Paradise Garage- which he described as “an amazing club”. It had lights, darkness, mus...</p>
                                      <span className="d-flex"><p className="col-md-6 p-0"><i className="fas fa-star text-warning"></i><i className="fas fa-star text-warning"></i><i className="fas fa-star text-warning"></i><i className="fas fa-star text-warning"></i><i className="fas fa-star text-warning"></i></p><p className="col-md-6 pl-lg-5">3 min read</p></span>
                                      <a href="#" >Read more</a>
                                    </div>
                                  </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4 p-2">
                        <div className="card shadow">
                            <div className="card-body">
                                <h4 className=" border-left">WHAT'S GROOVIN'?</h4>
                                <p className="card-text">What are Scales in Music: A Beginner’s 
                                    Guide & Types</p>
                                    <span className="d-flex text-mute border-bottom"><p><i className="far fa-clock"></i>10 June 2021</p><p><i className="fas fa-headphones-alt"></i>GrooveClasses</p></span>
                                    <p className="card-text">What are Scales in Music: A Beginner’s 
                                        Guide & Types</p>
                                        <span className="d-flex text-mute border-bottom"><p><i className="far fa-clock"></i>10 June 2021</p><p><i className="fas fa-headphones-alt"></i>GrooveClasses</p></span>
                                        <p className="card-text">What are Scales in Music: A Beginner’s 
                                            Guide & Types</p>
                                            <span className="d-flex text-mute border-bottom"><p><i className="far fa-clock"></i>10 June 2021</p><p><i className="fas fa-headphones-alt"></i>GrooveClasses</p></span>
                                            <p className="card-text">What are Scales in Music: A Beginner’s 
                                                Guide & Types</p>
                                                <span className="d-flex text-mute border-bottom"><p><i className="far fa-clock"></i>10 June 2021</p><p><i className="fas fa-headphones-alt"></i>GrooveClasses</p></span>
                                               
                            </div>
                        </div>
                        <div className="col-md-12 mt-3 p-0">
                                    <Image src={brochure} alt="brochure" className="card-img-top"/>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <footer className="page-footer font-small bg-footcolor">
            <div className="container text-center text-md-left">
                <div className="row">
                    <div className="col-md-3 mx-auto">
                                    <Image src={footerImg} alt="footer-img" className="card-img-top"/>

                            <ul className="list-unstyled">
                                <li>
                                <a href="#!">814, 8TH FLOOR,TOWER -1 ASSOTECH BUSINESS CRESTERRA PLOT NO. 22, SECTOR 135, NOIDA -201305
                                </a>
                                </li>
                            </ul>
                            <div className="col-md-12">
                                <span className="d-flex"><a href=""><i className="fab fa-facebook-f"></i><i className="fab fa-instagram"></i><i className="fab fa-twitter"></i><i className="fab fa-twitter"></i><i className="fab fa-youtube"></i></a></span>
                            </div>
                    </div>
                    <div className="col-md-3 mx-auto">
                        <h5 className="font-weight-bold text-uppercase text-white mt-3 mb-4">Information</h5>
                            <ul className="list-unstyled">
                                <li>
                                    <a href="#!">About Us</a>
                                </li>
                                <li>
                                    <a href="#!">Artists Services</a>
                                </li>
                                <li>
                                    <a href="#!">Write For Us</a>
                                </li>
                                <li>
                                    <a href="#!">Submit a song</a>
                                </li>
                                <li>
                                    <a href="#!">Blogs</a>
                                </li>
                            </ul>
                    </div>
                    <div className="col-md-3 mx-auto">
                        <h5 className="font-weight-bold text-white text-uppercase mt-3 mb-4">Helpful Links</h5>
                            <ul className="list-unstyled">
                                <li>
                                    <a href="#!">Privacy Policy</a>
                                </li>
                                <li>
                                    <a href="#!">PR Newswire</a>
                                </li>
                                <li>
                                    <a href="#!">Refund Policy</a>
                                </li>
                                <li>
                                    <a href="#!">Terms</a>
                                </li>
                                <li>
                                    <a href="#!">Contact Us</a>
                                </li>
                            </ul>
                    </div>
                    <div className="col-md-3 mx-auto">
                        <h5 className="font-weight-bold text-white text-uppercase mt-3 mb-4">Subscribe</h5>
                        <p>Subscribe to our newsletter for the latest updates</p>
                        <button className="btn-danger">Subscribe</button>
                    </div>
                </div>
            </div>
        </footer>
    </div>

  )
}


export default PostDetail

export async function getStaticProps({ ...ctx }) {
  const { slug } = ctx.params
  const content = await import(`../../posts/${slug}.md`)
  const config = await import(`../../data/config.json`)
  const data = matter(content.default)

  return {
    props: {
      siteTitle: config.title,
      frontmatter: data.data,
      markdownBody: data.content,
    },
  }
}

export async function getStaticPaths() {
  //get all .md files in the posts dir
  const blogs = glob.sync('posts/**/*.md')

  //remove path and extension to leave filename only
  const blogSlugs = blogs.map(file =>
    file
      .split('/')[1]
      .replace(/ /g, '-')
      .slice(0, -3)
      .trim()
  )

  // create paths with `slug` param
  const paths = blogSlugs.map(slug => `/post/${slug}`)
  return {
    paths,
    fallback: false,
  }
}
