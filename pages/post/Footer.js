import Image from 'next/image'; 
import footerImg from '../../public/images/footerImg.png';
import { FiFacebook, FiInstagram, FiTwitter, FiLinkedin, FiYoutube } from "react-icons/fi"; 


function Footer() {
  return (
            <div>
                <footer className="page-footer font-small bg-footcolor">
                    <div className="container text-start p-4 text-md-left">
                        <div className="row">
                            <div className="col-md-3 mx-auto">
                                            <Image src={footerImg} alt="footer-img" className="card-img-top"/>

                                    <ul className="list-unstyled">
                                        <li>
                                        <a href="#!" className="text-light">814, 8TH FLOOR,TOWER -1 ASSOTECH BUSINESS CRESTERRA PLOT NO. 22, SECTOR 135, NOIDA -201305
                                        </a>
                                        </li>
                                    </ul>
                                    <div className="col-md-12">
                                        <span className="d-flex "><a href=""><FiFacebook className="text-light mx-1"/><FiInstagram className="text-light mx-1"/><FiTwitter className="text-light mx-1"/><FiLinkedin className="text-light mx-1"/><FiYoutube className="text-light mx-1"/></a></span>
                                    </div>
                            </div>
                            <div className="col-md-3 mx-auto">
                                <h5 className="font-weight-bold text-uppercase text-light mt-3 mb-4">Information</h5>
                                    <ul className="list-unstyled">
                                        <li>
                                            <a href="#!" className="text-light my-1">About Us</a>
                                        </li>
                                        <li>
                                            <a href="#!" className="text-light my-1">Artists Services</a>
                                        </li>
                                        <li>
                                            <a href="#!" className="text-light my-1">Write For Us</a>
                                        </li>
                                        <li>
                                            <a href="#!" className="text-light my-1">Submit a song</a>
                                        </li>
                                        <li>
                                            <a href="#!" className="text-light my-1">Blogs</a>
                                        </li>
                                    </ul>
                            </div>
                            <div className="col-md-3 mx-auto">
                                <h5 className="font-weight-bold text-white text-uppercase mt-3 mb-4">Helpful Links</h5>
                                    <ul className="list-unstyled">
                                        <li>
                                            <a href="#!" className="text-light my-1">Privacy Policy</a>
                                        </li>
                                        <li>
                                            <a href="#!" className="text-light my-1">PR Newswire</a>
                                        </li>
                                        <li>
                                            <a href="#!" className="text-light my-1">Refund Policy</a>
                                        </li>
                                        <li>
                                            <a href="#!" className="text-light my-1">Terms</a>
                                        </li>
                                        <li>
                                            <a href="#!" className="text-light my-1">Contact Us</a>
                                        </li>
                                    </ul>
                            </div>
                            <div className="col-md-3 mx-auto">
                                <h5 className="font-weight-bold text-light text-uppercase mt-3 mb-4">Subscribe</h5>
                                <p className="text-light">Subscribe to our newsletter for the latest updates</p>
                                <button className="btn-danger btn">Subscribe</button>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
  )
}

export default Footer