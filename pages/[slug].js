import matter from 'gray-matter'
import Navbar from './post/Navbar';
import Footer from './post/Footer';
import Image from 'next/image';
import Detailimg from '../public/images/Detailimg.png'
// // import AccessTimeIcon from '@material-ui/icons'
// // import Icon from '@mui/material/Icon'
// import AccessTimeIcon from '@mui/icons-material/AccessTime'
// // import Icon from 'material-ui/Icon'
import { BiHeadphone, BiStopwatch } from "react-icons/bi"
import { BsFillArrowRightCircleFill } from "react-icons/bs"
import youtube from '../public/images/youtube.png'
import Imagecharlotte from '../public/images/Imagecharlotte.png'
import charlotte1 from '../public/images/charlotte1.png'
import brochure from '../public/images/brochure.png'
import Ellipse from '../public/images/Ellipse.png'
import {useState} from 'react'

const glob = require('glob')

function Detail(props) {
  console.debug(props.frontmatter,"data")
  const [detailList,setDetailList] =useState(props.allBlogs);
  return (
        <div>
          <Navbar />
<div className="container-fluid">
  <div className="row">
    <div className="col-md-12">
    <div className="container pt-5 mt-5">
  <div className="row">
    <div className="col-md-8 container p-2">
      {/* <div className=" row"> */}
      <Image src={props.frontmatter.image} alt="Picture"
      width={844} 
      height={325}/>
      {/* </div> */}
      <h5>{props.frontmatter.title}</h5>
      <div className="row">
        <div className="col-md-4">
          <p><span><BiStopwatch className="me-2"/>{props.frontmatter["Published on"]}</span></p>
        </div>
        <div className="col-md-4">
          <p><span><BiHeadphone className="me-2"/>Top Club</span></p>
        </div>
    <div className="col-md-4">
    < p><span><BiHeadphone className="me-2"/>Comment</span></p>
    </div>
  </div>
  <div className="col-md-12 pt-2">
    <p>{props.frontmatter.bodyContent}</p>
  </div>
  <h5>Table of Contents (best female DJs)</h5>
  <div className="card">
    <div className="card-body">
      <div className="row">
        <div className="col-md-6">
        <ul>
        <li className="text-primary"><span><BsFillArrowRightCircleFill className="bg-pink mx-2"/></span>Charlotte De Witte</li>
        <li className="text-primary"><span><BsFillArrowRightCircleFill className="bg-pink mx-2"/></span>Monika Kruse</li>
        <li className="text-primary"><span><BsFillArrowRightCircleFill className="bg-pink mx-2"/></span>Alison Wonderland</li>
        <li className="text-primary"><span><BsFillArrowRightCircleFill className="bg-pink mx-2 "/></span>Nastia</li>
        <li className="text-primary" ><span><BsFillArrowRightCircleFill className="bg-pink mx-2"/></span>Nastia</li>
      </ul>
        </div>
        <div className="col-md-6">
        <ul>
        <li className="text-primary"><span><BsFillArrowRightCircleFill className="bg-pink mx-2"/></span>REZZ</li>
        <li className="text-primary"><span><BsFillArrowRightCircleFill className="bg-pink mx-2"/></span>Anja Schneider</li>
        <li className="text-primary"><span><BsFillArrowRightCircleFill className="bg-pink mx-2"/></span>Alison Wonderland</li>
        <li className="text-primary"><span><BsFillArrowRightCircleFill className="bg-pink mx-2"/></span>Nastiae</li>
        <li className="text-primary"><span><BsFillArrowRightCircleFill className="bg-pink mx-2"/></span>Nastia</li>
      </ul>
        </div>
      </div>
    </div>
  </div>
  <h5 className="pt-3 pb-1">Best Female DJs You Should Know:</h5>
  <Image src={youtube} alt="youtube" layout="responsive" className="img" />
  <div className="row p-2">
    <div className="col-md-12">
      <div className="card">
        {/* <div className="card-header">
          <h6><span className="pe-1">1.</span>Charlotte De Witte</h6>
        </div> */}
        <div className="card-body">
        <h6><span className="pe-1">1.</span>Charlotte De Witte</h6>
          <div className="row">
            <div className="col-md-6">
            <Image src={Imagecharlotte} alt="Imagecharlotte" layout="responsive" />
            </div>
            <div className="col-md-6">
            <Image src={charlotte1} alt="charlotte1" layout="responsive" />
            </div>
          </div>
          <div className="col-md-12 pt-2">
    <p>Electronic dance music is surging to the forefront of mainstream success and getting popular mainly among youth. There are so many industries including music that are dominated by men. However, the contribution of females in music industry is increasing steadily each year. The female DJs are always present in the music industry since 1974.
      </p>
     <p> Karen Mixon Cook in 1974 became the first professional female DJ in the world. Since then, women have taken on the male-dominated world of electronic music to show that they are just as capable of producing great mixes and playlists. They perform at major events around the world, selling their own albums and some have even started their own record labels.
      </p>

    <p>Being a female DJ in a scene partially dominated by men is somewhat a difficult task. A few female DJs in the world have gained the popularity they deserved. The female DJs listed here are some of the leading ladies in the electronic dance music industry.The following is a list of 15 best female DJs around the world:</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div className="row p-2">
  {/* {blogList.map(d=>{
                                return (
                                    <Link key={d.slug} href={{ pathname: `/posts/${d.slug}` }}>
                                    {d? <De data={d}/>:""}
                                    </Link>)
                                })} */}
  </div>
  <div className="row p-2">
    <div className="col-md-12 ">
      <div className="card p-3">
      <div className="card-body">
        <h6 className="mb-5">Post Comment</h6>
        <div className="row">
          <div className="col-md-12">
            <div className="card border-0">
              <div className="row">
                <div className="col-md-4">
                <Image src={Ellipse} alt="Ellipse" layout="responsive"/>
                </div>
                <div className="col-md-8">
                  <h6>Rosalina Kelian</h6>
                  < p className="" ><span><BiStopwatch className="me-2"/>10 June 2021</span></p>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lacus, sit sed massa fames purus. A egestas vehicula nunc massa eget. Nullam dolor sit blandit diam, fringilla arcu ipsum purus potenti.</p>
                </div>
                <br/>
                <div className="border-bottom pt-2 "></div>
              </div>
              <div className="row pt-5">
                <div className="col-md-4">
                <Image src={Ellipse} alt="Ellipse" layout="responsive"/>
                </div>
                <div className="col-md-8">
                  <h6>Rosalina Kelian</h6>
                  < p className="" ><span><BiStopwatch className="me-2"/>10 June 2021</span></p>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lacus, sit sed massa fames purus. A egestas vehicula nunc massa eget. Nullam dolor sit blandit diam, fringilla arcu ipsum purus potenti.</p>
                </div>
                <br/>
                <div className="border-bottom pt-2 "></div>
              </div>
              <div className="row pt-5">
                <div className="col-md-4">
                <Image src={Ellipse} alt="Ellipse" layout="responsive"/>
                </div>
                <div className="col-md-8">
                  <h6>Rosalina Kelian</h6>
                  < p className="" ><span><BiStopwatch className="me-2"/>10 June 2021</span></p>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lacus, sit sed massa fames purus. A egestas vehicula nunc massa eget. Nullam dolor sit blandit diam, fringilla arcu ipsum purus potenti.</p>
                </div>
                <br/>
              </div>
            </div>
          </div>
        </div>
      </div>
      </div>
    </div>
  </div>
  <h5 className="pt-3 pb-1">Leave A Comment</h5>
  <div className="row pt-2 pb-2">
    <div className="col-md-6">
    <div class="form-floating">
      <textarea class="form-control" id="comment" name="text" placeholder="Comment goes here"></textarea>
      <label for="comment">Name</label>
    </div>
    </div>
    <div className="col-md-6">
    <div class="form-floating">
      <textarea class="form-control" id="comment" name="text" placeholder="Comment goes here"></textarea>
      <label for="comment">Email</label>
    </div>
    </div>
    <div className="col-md-12 pt-2 pb-2">
    <div class="form-floating">
      <textarea class="form-control" id="comment" name="text" placeholder="Comment goes here"></textarea>
      <label for="comment">Comment</label>
      <button className="bg-danger btn mt-4 text-white">Post Comment</button>
    </div>
    
    </div>
  </div>
  </div>
<div className="col-md-4 p-2">
                <div className="card shadow">
                    <div className="card-body">
                        <h4 className=" border-left">WHAT'S GROOVIN'?</h4>
                        <p className="card-text">What are Scales in Music: A Beginner's 
                            Guide and Types</p>
                            <span className="d-flex text-mute border-bottom"><p><BiStopwatch className="me-2"/>10 June 2021</p><p className="ms-1"><BiHeadphone className="me-2"/>GrooveClasses</p></span>
                            <p className="card-text">What are Scales in Music: A Beginner's 
                                Guide and Types</p>
                                <span className="d-flex text-mute border-bottom"><p><BiStopwatch className="me-2"/>10 June 2021</p><p className="ms-1"><BiHeadphone className="me-2"/>GrooveClasses</p></span>
                                <p className="card-text">What are Scales in Music: A Beginner's 
                                    Guide and Types</p>
                                  <span className="d-flex text-mute border-bottom"><p><BiStopwatch className="me-2"/>10 June 2021</p><p className="ms-1"><BiHeadphone className="me-2"/>GrooveClasses</p></span>
                                    <p className="card-text">What are Scales in Music: A Beginner's 
                                        Guide and Types</p>
                                        <span className="d-flex text-mute border-bottom"><p><BiStopwatch className="me-2"/>10 June 2021</p><p className="ms-1"><BiHeadphone className="me-2"/>GrooveClasses</p></span>
                                    
                    </div>
                </div>
                <div className="col-md-12 mt-3 p-0">
                            <Image src={brochure} alt="brochure" className="card-img-top"/>

                </div>
  </div>
</div>
</div>
    </div>
  </div>
</div>
<Footer />   
        </div>
  )
}

export default Detail

export async function getStaticProps({ ...ctx }) {
  const { slug } = ctx.params
  const content = await import(`../posts/${slug}.md`)
  const config = await import(`../data/config.json`)
  const data = matter(content.default)
  
  
  
  return {
  props: {
  siteTitle: config.title,
  frontmatter: data.data,
  markdownBody: data.content,
  },
  }
  }
  
  
  
  export async function getStaticPaths() {
  //get all .md files in the posts dir
  // const blogs = glob.sync('posts/**/*.md')
  const blogs = glob.sync('posts/**/*.md')
  
  
  console.debug("getStaticPaths: blogs",blogs)
  //remove path and extension to leave filename only
  const blogSlugs = blogs.map(file =>
  file
  .split('/')[1]
  .replace(/ /g, '-')
  .slice(0, -3)
  .trim()
  )
  
  
  
  // create paths with `slug` param
  const paths = blogSlugs.map(slug => `/${slug}`)
  return {
  paths,
  fallback: false,
  }
  }

// export async function getStaticProps() {
//   const siteConfig = await import(`../data/config.json`)
//   //get posts & context from folder
//   const posts = (context => {
//     const keys = context.keys()
//     const values = keys.map(context)

//     const data = keys.map((key, index) => {
//       // Create slug from filename
//       const Detail = key
//         .replace(/^.*[\\\/]/, '')
//         .split('.')
//         .slice(0, -1)
//         .join('.')
//       const value = values[index]
//       // Parse yaml metadata & markdownbody in document
//       const document = matter(value.default)
//       return {
//         frontmatter: document.data,
//         markdownBody: document.content,
//         Detail,
//       }
//     })
//     return data
//   })(require.context('../content', true, /\.md$/))

//   return {
//     props: {
//       allDetails: posts,
//       title: siteConfig.default.title,
//       description: siteConfig.default.description,
//     },
//   }
// }
  
//   export async function getStaticPaths() {
//     //get all .md files in the posts dir
//     const Detail = glob.sync('content/**/*.md')
  
//     //remove path and extension to leave filename only
//     const detailList = detailList.map(file =>
//       file
//         .split('/')[1]
//         .replace(/ /g, '-')
//         .slice(0, -3)
//         .trim()
//     )
  
//     // create paths with `slug` param
//     const paths = detailList.map(Detail => `/content/${Detail}`)
//     return {
//       paths,
//       fallback: false,
//     }
    
//   }
  // return {
  //   props: {
  //     allDetail: posts,
  //     title: siteConfig.default.title,
  //     description: siteConfig.default.description,
  //   },
  // }
  