import Image from 'next/image';
// import ministryOfSound from '../public/images/ministryOfSound.png'; 
import Link from 'next/link'
import { AiFillStar } from "react-icons/ai";
import { BiAlarm, BiHeadphone, BiRightArrowAlt } from "react-icons/bi";
  

function BlogListcard(props){
    console.debug("BlogListcard",props.data);
    // var res = str.substring(0, 4);
    return (
        <div className="col-md-6 p-2">
            <div className="card">
                <Image src={props.data.frontmatter.image} alt="Picture" 
                width={500}        
                height={350}/>

                <div className="card-body">
                    <span className="d-flex"><p className="me-5"><BiAlarm className="me-2"/>{props.data.frontmatter["Published on"]}</p><p><BiHeadphone className="me-2"/>Top Club</p></span>
                    <Link href={ props.data.slug} >
                      <a className="text-decoration-none"> <h6 className="card-title text-black ">{props.data.frontmatter.title}</h6></a>
                    </Link> 
                    <p className="card-text">{props.data.frontmatter.summary}</p>
                    <span className="d-flex"><p className="col-md-6 p-0"><AiFillStar className="text-warning"/><AiFillStar className="text-warning"/><AiFillStar className="text-warning"/><AiFillStar className="text-warning"/><AiFillStar className="text-warning"/></p><p className="col-md-6 text-end pl-lg-5">3 min read</p></span>
                    {/* <Link >
                    <a href="/Detail" >Read more</a>
                    </Link> */}
                    <Link href={{ pathname: props.data.slug, query: { name: 'Detail' } }}>
                        <a className="text-decoration-none text-black">Read more<BiRightArrowAlt/></a>
                    </Link>
                </div>
            </div>
        </div>
    )
}

export default BlogListcard;
